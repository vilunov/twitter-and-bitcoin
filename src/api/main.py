from src.api.trailing_stop_loss import StopTrail
import argparse


def main(options):
    task = StopTrail(options.size, options.interval)
    task.run()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--size', type=float,
                        help='How many satoshis (or USD) the stop loss should be placed above or below current price', default=2)
    parser.add_argument('--interval', type=float, help="How often the bot should check for price changes", default=5)

    options = parser.parse_args()
    main(options)
