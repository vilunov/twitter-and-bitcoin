from poloniex import Poloniex

from src.api.api import predict


class PoloniexAPI:

    def __init__(self, api_key=None, api_secret=None):
        self.btc = 0
        self.usdt = 1000
        self.client = Poloniex()

    def buy(self, market, amount, price):
        self.btc = amount / price
        self.usdt = 0

    def sell(self, market, amount, price):
        self.usdt = amount * price
        self.btc = 0

    def _buy(self, market, amount, price):
        return self.client.buy(market, price, amount)

    def _sell(self, market, amount, price):
        return self.client.sell(market, price, amount)

    def get_price(self, market):
        return float(self.client.returnTicker()[market]['last'])

    def get_balance(self, coin):
        if coin == 'USDT':
            return self.usdt
        else:
            return self.btc

    def _get_balance(self, coin):
        return float(self.client.returnBalances()[coin])


# if __name__ == '__main__':
#     prediction = predict()
#
#     while prediction:
#         if prediction > 0:
#             buy()
#         else:
#             sell()
#
#         BTC_PRICE = BTC_PRICE*(prediction/100 + 1)
#         prediction = predict()
#
#     sell()
#     print(BALANCE_USDT)
