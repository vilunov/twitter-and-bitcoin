from src.api.trading_api import PoloniexAPI
import time
import logging

logging.basicConfig(handlers=[
    logging.FileHandler("log.log"),
    logging.StreamHandler()
],
    level=logging.DEBUG,
    format='%(levelname)s: %(asctime)s %(message)s',
    datefmt='%m/%d/%Y %I:%M:%S')


class StopTrail:

    def __init__(self, stopsize, interval, market='USDT_BTC', type='buy'):
        self.poloniex = PoloniexAPI()
        self.market = market
        self.type = type
        self.stopsize = stopsize
        self.interval = interval
        self.stoploss = self.initialize_stop()

    def initialize_stop(self):
        if self.type == "buy":
            return self.poloniex.get_price(self.market) + self.stopsize
        else:
            return self.poloniex.get_price(self.market) - self.stopsize

    def update_stop(self):
        price = self.poloniex.get_price(self.market)
        if self.type == "sell":
            if (price - self.stopsize) > self.stoploss:
                self.stoploss = price - self.stopsize
                print("New high observed: Updating stop loss to %.8f" % self.stoploss)
            elif price <= self.stoploss:
                self.type = "buy"
                amount = self.poloniex.get_balance(self.market.split("_")[1])
                price = self.poloniex.get_price(self.market)
                self.poloniex.sell(self.market, amount, price - 5)
                print("Sell triggered | Price: %.8f | Stop loss: %.8f" % (price, self.stoploss))
                self.stoploss = self.initialize_stop()
        elif self.type == "buy":
            if (price + self.stopsize) < self.stoploss:
                self.stoploss = price + self.stopsize
                print("New low observed: Updating stop loss to %.8f" % self.stoploss)
            elif price >= self.stoploss:
                self.type = "sell"
                price = self.poloniex.get_price(self.market)
                amount = self.poloniex.get_balance(self.market.split("_")[0])  # 0.20% maker/taker fee
                self.poloniex.buy(self.market, amount, price)
                print("Buy triggered | Price: %.8f | Stop loss: %.8f" % (price, self.stoploss))
                self.stoploss = self.initialize_stop()

    def print_status(self):
        last = self.poloniex.get_price(self.market)
        logging.info("---------------------")
        logging.info(f"Trail type: {self.type}")
        logging.info(f"Market: {self.market}")
        logging.info(f"Stop loss: {self.stoploss}")
        logging.info(f"Last price: {last}")
        logging.info(f"Stop size: {self.stopsize}")
        logging.info(f'Available balance BTC: {self.poloniex.get_balance(self.market.split("_")[1])}')
        logging.info(f'Available balance USDT: {self.poloniex.get_balance(self.market.split("_")[0])}')
        logging.info("---------------------")

    def run(self):
        while True:
            self.print_status()
            self.update_stop()
            time.sleep(self.interval)
