import torch

from src.constants import PATH_DATA

predictions = torch.load(PATH_DATA / "dataset.pickle")
X, y = predictions.tensors
Y = []
for label in y:
    for change in label:
        if change != 0:
            Y.append(change)
counter = 0


def predict():
    global counter
    counter += 1
    if counter < len(Y):
        return Y[counter]
    else:
        return None

