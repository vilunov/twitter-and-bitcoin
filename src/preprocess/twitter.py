import os
import bz2
import json
from pathlib import Path

import pandas as pd

from tqdm import tqdm
from constants import PATH_TWITTER, PATH_DATA


def read_file(file):
    with bz2.open(file) as f:
        k = []
        a = f.readline()
        while a:
            k.append(json.loads(a))
            a = f.readline()
        data_tweets = pd.DataFrame(k)
    return data_tweets


def preprocess_file(file):
    data_tweets = read_file(file)
    print(len(data_tweets))
    data_tweets = data_tweets[~data_tweets["delete"].notnull()]
    print(len(data_tweets))
    data_tweets = data_tweets[~data_tweets["retweeted_status"].notnull()]
    print(len(data_tweets))
    data_tweets = data_tweets[data_tweets["user"].notnull()]
    data_tweets = data_tweets[data_tweets["id_str"].notnull()]
    print(len(data_tweets))

    data_tweets = data_tweets[data_tweets["lang"] == "en"]
    data_tweets = data_tweets[["id_str", "text", "timestamp_ms", "user"]]
    data_tweets["followers"] = data_tweets["user"].apply(lambda x: x["followers_count"])
    data_tweets["user"] = data_tweets["user"].apply(lambda x: x["id_str"])
    return data_tweets.set_index("id_str")


def files():
    for root, folders, files in os.walk(PATH_TWITTER):
        for file in files:
            if file.endswith(".json.bz2"):
                yield os.path.join(root, file)


def preprocess():
    folder = Path(PATH_DATA / "twitter")
    file_list = sorted(files())
    for path in tqdm(file_list):
        df = preprocess_file(path)
        subpath = path[len(PATH_TWITTER):-8] + "csv"
        target = folder / subpath
        target.parent.mkdir(parents=True, exist_ok=True)
        df.to_csv(target, quoting=1)


if __name__ == '__main__':
    preprocess()
