import glob

import numpy as np
import pandas as pd
from tqdm import tqdm

from constants import PATH_DATA
from .embedding import embed_dataframe

l = sorted(glob.glob(str(PATH_DATA / "twitter/2018/01/**/*.csv"), recursive=True))
df = pd.read_csv(l[0])
a = np.array_split(df, len(df) // 20 + 1)

for df_ in tqdm(a):
    embed_dataframe(df_)

df = pd.concat(a)
