import warnings

import pandas as pd

from constants import PATH_DATA, PATH_COINBASE

warnings.filterwarnings("ignore")
timestamp = 1


def preprocess():
    dataframe = pd.read_csv(PATH_COINBASE)

    df = dataframe[['Timestamp', 'Weighted_Price']]
    df['Timestamp'] = pd.to_datetime(dataframe['Timestamp'], unit='s')
    df_january = df[(df['Timestamp'] >= '2018-01-01 00:00:00') & (df['Timestamp'] <= '2018-01-31 00:00:00')]
    df_february = df[(df['Timestamp'] >= '2018-02-01 00:00:00') & (df['Timestamp'] <= '2018-02-28 00:00:00')]
    df_september = df[(df['Timestamp'] >= '2018-09-01 00:00:00') & (df['Timestamp'] <= '2018-09-30 00:00:00')]
    df_october = df[(df['Timestamp'] >= '2018-10-01 00:00:00') & (df['Timestamp'] <= '2018-10-31 00:00:00')]

    dataframes = {"january": df_january, "february": df_february, "september": df_september, "october": df_october}

    for (name, df) in dataframes.items():
        df['Change'] = pd.Series(df['Weighted_Price']).pct_change() * 100
        df = df.iloc[::timestamp, :]
        with open(PATH_DATA / 'btc_price_dfs' / f'{name}_{timestamp}.csv', 'w') as f:
            df.to_csv(f, sep=',', index=False)


if __name__ == '__main__':
    preprocess()
