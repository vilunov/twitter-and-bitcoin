from flair.embeddings import WordEmbeddings, FlairEmbeddings, BertEmbeddings, DocumentPoolEmbeddings, Sentence

glove = WordEmbeddings('en-twitter')

#flair_forward_embedding = FlairEmbeddings('multi-forward')
#flair_backward_embedding = FlairEmbeddings('multi-backward')

# bert_embedding = BertEmbeddings(layers="-1,-2")


embeddings = [
    #flair_forward_embedding,
    #flair_backward_embedding,
    #bert_embedding,
    glove,
]
embedding_size = sum(i.embedding_length for i in embeddings)
document_embeddings = DocumentPoolEmbeddings(embeddings)

print("Embedding vector size:", embedding_size)


def embed_dataframe(df):
    sentences = df["text"].apply(Sentence)
    document_embeddings.embed(sentences)
    df["vec"] = sentences.apply(Sentence.get_embedding)
    del df["text"]
    return df
