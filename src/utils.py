import datetime
import calendar
import glob
import pandas as pd
import numpy as np
import torch
from torch.utils.data import TensorDataset

from constants import PATH_DATA
from tqdm import tqdm

from preprocess.embedding import embed_dataframe

period_train = 24 * 60 * 15
period_val = 24 * 60 * 15


def read_twitter(*, is_validation=False):
    dfs = list()
    files = sorted(glob.glob(str(PATH_DATA / "twitter/2018/01/**/*.csv"), recursive=True))
    if is_validation:
        files = files[period_train:period_train + period_val]
    else:
        files = files[:period_train]
    for i in tqdm(files, desc="Reading Twitter data"):
        df = pd.read_csv(i)
        dfs.append(df)
    dfs = pd.concat(dfs)
    dfs['timestamp'] = (
        pd.to_datetime(dfs.timestamp_ms, unit="ms").astype("datetime64[m]") +
        datetime.timedelta(hours=-12)
    )
    dfs = dfs.drop(columns=["timestamp_ms"]).set_index("id_str")
    print(f"Len: {len(dfs)}")
    return dfs


def read_bitcoin():
    months = [1, 2, 9, 10]
    months = {i: calendar.month_name[i].lower() for i in months}
    res = {
        i: pd.read_csv(PATH_DATA / 'btc_price_dfs' / f'{j}_1.csv')
        for i, j in months.items()
    }
    return res


def combine_and_group(twitter, bitcoin):
    """
    :param bitcoin: dict(month_number -> DataFrame of bitcoin prices)
    :param twitter: DataFrame of all twitter posts
    :return: dict(timestamp -> dict).
    Children are dict("tweets" -> df, "label" -> label, "prev_label" -> prev_label, "dim" -> tensor)
    """
    grouped = {i: dict(tweets=j) for i, j in twitter.groupby("timestamp")}
    for timestamp, df in grouped.items():
        btc_data = bitcoin[timestamp.month]
        btc_data.Timestamp = btc_data.Timestamp.astype("datetime64[m]")
        selector = (btc_data.Timestamp == timestamp)
        if not any(selector):
            continue
        label = btc_data.loc[selector].iloc[0]['Change']
        df['label'] = label
    for i in list(grouped):
        if 'label' not in grouped[i]:
            del grouped[i]
    return grouped


def create_sample_timeseries(start, values, length, forward, batch_size=10):
    """
    :param values:
    :param length: length of a sample in minutes
    :return: output shapes:
    X - (length, vec_size)
    y - (length + forward,)
    """
    sample = values[start:start + length]
    shifted_labels = [i["label"] for i in values[start - 1: start + length - 1]]

    # lazy vectorization of tweets and tweet-minutes
    # We don't do this before sampling, because we might now use a lot of our data. Vectorization is very
    # time-consuming, so we do this only when we need to and we don't repeat calculations
    for i in sample:
        if "vec" in i:
            continue
        df = i["tweets"]
        vecs = list()
        followers = list()
        norm_const = torch.Tensor(list(df.followers)).sqrt().sum()
        for j in np.array_split(df, len(df) // batch_size + 1):
            embed_dataframe(j)
            vecs += list(j["vec"])
            followers += list(j["followers"])
        weight_coef = torch.Tensor(followers).sqrt() / norm_const
        i["vec"] = (torch.stack(vecs).transpose(dim0=1, dim1=0) * weight_coef).sum(dim=1)

    X = np.array([
        np.array(list(i["vec"]) + [k])
        for i, k in zip(sample, shifted_labels)
    ])
    y = np.array([i["label"] for i in values[start:start + length + forward]])
    return X, y


def create_dataset(data, *, length, forward, n_samples):
    keys = sorted(data.keys())
    values = [data[i] for i in keys]
    data_X, data_y = [], []
    print(f"len(values) = {len(values)}, length = {length}, forward = {forward}")
    start_timestamps = np.random.choice((len(values) - length - forward - 2) // 15, n_samples, replace=False) * 15 + 1
    for start in tqdm(start_timestamps, desc="Sampling sequences"):
        sample_x, sample_y = create_sample_timeseries(start, values, length, forward)
        data_X.append(sample_x)
        data_y.append(sample_y)
    data_X = torch.FloatTensor(data_X)
    data_y = torch.FloatTensor(data_y)
    print("Dataset created, shapes:", data_X.shape, data_y.shape)
    return TensorDataset(
        data_X.cpu(),
        data_y.cpu(),
    )

