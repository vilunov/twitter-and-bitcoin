import torch
from torch import nn, optim
from torch.utils.data import DataLoader
from tqdm import tqdm

from constants import PATH_DATA


class TweetBinaryClassifier(nn.Module):
    def __init__(self, *, batch_size, max_sequence_length, vec_size, rnn_hidden=(8,), gru=False, pred_length=2):
        """
        Input parameters:
        :param batch_size:           size of the vector batch
        :param max_sequence_length:  size of maximum sequence length, i.e. the number of minutes
        :param vec_size:             size of one tweet-minute-vector, i.e. the mean of all tweet-vectors in a minute
        Hyperparameters:
        :param rnn_hidden:          number of hidden LSTM units per each LSTM layer
        """
        super().__init__()
        # input params
        self._batch_size = batch_size
        self._max_sequence_length = max_sequence_length
        self._vec_size = vec_size
        self._pred_length = pred_length
        # submodules
        s = vec_size + 1  # + label
        rnn_class = nn.GRU if gru else nn.LSTM
        self.rnns = nn.ModuleList()
        for hidden in rnn_hidden[:-1]:
            self.rnns.append(rnn_class(s, hidden, batch_first=True, dropout=0.8))
            s = hidden
        self.rnns.append(rnn_class(s, rnn_hidden[-1], batch_first=True))
        self.linr = nn.Linear(rnn_hidden[-1], pred_length)
        self.actv = nn.Sigmoid()

    def forward(self, batch_input, series_num):
        """

        :param batch_input:  batch of input data of shape (batch_size, max_sequence_length, vec_size)
        :return:             the class predictions of shape (batch_size, max_sequence_length, pred_length)
        """
        v = batch_input
        for rnn in self.rnns:
            v, _ = rnn(v)  # last shape: (batch_size, max_sequence_length, lstm_hidden)
        v = self.linr(v[:, -series_num:])
        v = self.actv(v)
        return v

    def validate(self, criterion, data, label, *, series_num, pred_length):
        self.eval()
        in_length = data.shape[1]
        label = label[:, (in_length - series_num + 1):(in_length + pred_length)].unfold(1, pred_length, 1)
        output = self(data, series_num=series_num)
        loss2 = criterion(output, (label > 0).float())
        return loss2.data.item()

    def fit(self, dataset, *, series_num, epochs, validation=None):
        print("Entered train")
        optimizer = optim.Adam(
            self.parameters(),
            lr=5e-4,
        )
        criterion_bce = nn.BCELoss()

        print("Creating dataset")
        dataloader = DataLoader(dataset, batch_size=self._batch_size, shuffle=True, num_workers=0)

        pred_length = self._pred_length
        print("Training model")
        pbar = tqdm(total=epochs, desc="Training model")
        loss1, loss2 = [], []

        for epoch in range(epochs):
            for data, label in dataloader:
                data = data.cuda()
                label = label.cuda()
                optimizer.zero_grad()

                in_length = data.shape[1]
                output = self(data, series_num=series_num)
                label = label[:, (in_length - series_num + 1):(in_length + pred_length)].unfold(1, pred_length, 1)

                loss = criterion_bce(output, (label > 0).float())
                loss.backward()
                optimizer.step()
            pbar.update()
            loss1.append(loss.data.item())
            postfix = dict(loss=loss.data.item())
            if validation is not None:
                _t = self.validate(
                    criterion_bce,
                    *validation,
                    series_num=series_num,
                    pred_length=pred_length,
                )
                loss2.append(_t)
                postfix["loss_val"] = _t
                print(postfix["loss_val"])
                self.train()
            pbar.set_postfix(postfix)
            if (epoch % 100) == 99:
                torch.save(self, PATH_DATA / "models" / f"model_{epoch + 1}.pickle")
        pbar.close()
        return loss1, loss2
