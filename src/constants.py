import os
from pathlib import Path

from preprocess.embedding import embedding_size as VEC_SIZE

__all__ = (
    "VEC_SIZE",
    "PATH_DATA",
    "PATH_COINBASE",
    "PATH_TWITTER",
    "BATCH_SIZE",
    "SEQ_LEN",
    "FORWARD_PREDICTION_LEN",
    "EPOCHS",
)


PATH_DATA = Path(os.path.dirname(__file__)) / '..' / 'data'
PATH_COINBASE = PATH_DATA / 'raw_data_coinbase.csv'
PATH_TWITTER = (
    "/mnt/storage/Downloads/archiveteam-twitter-stream-2018-01/archiveteam-twitter-stream-2018-01/"
)

BATCH_SIZE = 1000
SEQ_LEN = 60 * 4  # length of one training sample in a sequence, in minutes
FORWARD_PREDICTION_LEN = 1  # number of prediction to make for each minute, in minutes
EPOCHS = 20000
