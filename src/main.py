import torch
import torch.cuda
from ignite.engine import create_supervised_evaluator
from ignite.metrics import MeanSquaredError
from torch.utils.data import DataLoader
import matplotlib.pyplot as plt

from constants import PATH_DATA, SEQ_LEN, BATCH_SIZE, VEC_SIZE, EPOCHS, FORWARD_PREDICTION_LEN
from models.tweet import TweetBinaryClassifier


def prepare_dataset():
    from utils import read_twitter, combine_and_group, create_dataset, read_bitcoin
    twitter = read_twitter()
    twitter_val = read_twitter(is_validation=True)
    bitcoin = read_bitcoin()

    data = combine_and_group(twitter, bitcoin)
    data_val = combine_and_group(twitter_val, bitcoin)

    dataset = create_dataset(data, length=SEQ_LEN, n_samples=1000, forward=FORWARD_PREDICTION_LEN)
    torch.save(dataset, PATH_DATA / "dataset.pickle")
    dataset = create_dataset(data_val, length=SEQ_LEN, n_samples=100, forward=FORWARD_PREDICTION_LEN)
    torch.save(dataset, PATH_DATA / "dataset_test.pickle")


def train_model():
    dataset = torch.load(PATH_DATA / "dataset.pickle")
    validation_x, validation_y = torch.load(PATH_DATA / "dataset_test.pickle").tensors
    validation_x = validation_x.cuda()[:BATCH_SIZE]
    validation_y = validation_y.cuda()[:BATCH_SIZE]

    model = TweetBinaryClassifier(
        batch_size=BATCH_SIZE,
        max_sequence_length=SEQ_LEN,
        vec_size=VEC_SIZE,
        pred_length=FORWARD_PREDICTION_LEN,
    ).cuda()
    l1, l2 = model.fit(
        dataset,
        series_num=128,
        epochs=EPOCHS,
        validation=(validation_x, validation_y),
    )
    torch.save(model.cpu(), PATH_DATA / "models" / "model.pickle")
    return l1, l2


def evaluate(plot=False):
    model = torch.load(PATH_DATA / "models" / "model.pickle")
    dataset_test = torch.load(PATH_DATA / "dataset_test.pickle")

    model.eval()
    model.cpu()

    if plot:
        x = dataset_test.tensors[0].cpu()
        y = dataset_test.tensors[1].cpu()
        y_ = model(x)
        for i in range(3):
            plt.plot(list(y_[i] - y[i]))
            plt.show()

    dataloader = DataLoader(dataset_test, batch_size=50, shuffle=True, num_workers=0)

    metrics = {'mean_squared_error': MeanSquaredError()}
    val_evaluator = create_supervised_evaluator(model, metrics=metrics)
    metrics = val_evaluator.run(dataloader).metrics
    print(f"Test Results - MeanSquaredError: {metrics['mean_squared_error']}")


if __name__ == '__main__':
    print("cuda:", torch.cuda.is_available())

    # prepare_dataset()
    l1, l2 = train_model()
    # evaluate(True)
